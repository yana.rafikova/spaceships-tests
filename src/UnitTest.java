import org.junit.jupiter.api.*;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class UnitTest {

    private ArrayList<Spaceship> testList = new ArrayList<>();
    CommandCenter commandCenter = new CommandCenter();

    @AfterEach
    public void clear() {
        testList.clear();
    }

    @DisplayName("Возвращает самый хорошо вооруженный корабль (с самой большой огневой мощью, отличной от нуля)")
    @Test
    public void test1() {
        testList.add(new Spaceship("1", 100, 0,0));
        testList.add(new Spaceship("2", 0, 0,0));

        Spaceship result = commandCenter.getMostPowerfulShip(testList);

        Assertions.assertEquals(result.getFirePower(), 100);

    }

    @DisplayName("Возвращает первый по списку максимально вооруженный корабль")
    @Test
    public void test2() {
        testList.add(new Spaceship("1", 100, 0, 0));
        testList.add(new Spaceship("2", 100, 0, 0));
        testList.add(new Spaceship("3", 0, 0, 0));

        Spaceship result = commandCenter.getMostPowerfulShip(testList);

        Assertions.assertEquals(result.getName(), "1");

    }

    @DisplayName("Возвращает null, если нет вооруженных кораблей")
    @Test
    public void test3() {
        testList.add(new Spaceship("1", 0, 0, 0));
        testList.add(new Spaceship("2", 0, 0, 0));

        Spaceship result = commandCenter.getMostPowerfulShip(testList);

        Assertions.assertNull(result);

    }

    @DisplayName("Возвращает корабль с заданным именем")
    @Test
    public void test4() {

        testList.add(new Spaceship("1", 0, 0, 0));
        testList.add(new Spaceship("2", 0, 0, 0));

        Spaceship result = commandCenter.getShipByName(testList, "1");

        Assertions.assertEquals(result.getName(), "1");
    }

    @DisplayName("Null, если корабля с заданным именем нет")
    @Test
    public void test5() {
        testList.add(new Spaceship("1", 100, 0, 0));

        Spaceship result = commandCenter.getShipByName(testList, "2");

        Assertions.assertNull(result);
    }

    @DisplayName("Возвращает только корабли с достаточно большим грузовым трюмом для перевозки груза заданного размера")
    @Test
    public void test6() {
        testList.add(new Spaceship("1", 100, 30, 0));
        testList.add(new Spaceship("2", 100, 0, 0));
        testList.add(new Spaceship("3", 100, 40, 0));

        ArrayList<Spaceship> result = commandCenter.getAllShipsWithEnoughCargoSpace(testList, 20);

        Assertions.assertEquals(result.get(0).getName(),"1");
        Assertions.assertEquals(result.get(1).getName(),"3");

    }

    @DisplayName("Возвращает пустой список, если нет кораблей с достаточным грузовым трюмом")
    @Test
    public void test7() {
        testList.add(new Spaceship("1", 100, 30, 0));
        testList.add(new Spaceship("2", 100, 0, 0));
        testList.add(new Spaceship("3", 100, 40, 0));

        ArrayList<Spaceship> result = commandCenter.getAllShipsWithEnoughCargoSpace(testList, 50);

        Assertions.assertTrue(result.isEmpty());

    }

    @DisplayName("Возвращает только \"мирные\" корабли")
    @Test
    public void test8() {
        testList.add(new Spaceship("1", 0, 30, 0));
        testList.add(new Spaceship("2", 100, 0, 0));
        testList.add(new Spaceship("3", 0, 40, 0));

        ArrayList <Spaceship> result = commandCenter.getAllCivilianShips(testList);

        Assertions.assertEquals(result.get(0).getName(), "1");
        Assertions.assertEquals(result.get(1).getName(), "3");

    }

    @DisplayName("Возвращает пустой список, если мирных кораблей нет")
    @Test
    public void test9() {
        testList.add(new Spaceship("First", 200, 30, 0));
        testList.add(new Spaceship("Second", 100, 0, 0));
        testList.add(new Spaceship("Third", 100, 40, 0));

        ArrayList <Spaceship> result = commandCenter.getAllCivilianShips(testList);

        Assertions.assertTrue(result.isEmpty());

    }

}
