import java.util.ArrayList;

public class Test {

    ArrayList<Spaceship> testList = new ArrayList<>();
    CommandCenter commandCenter = new CommandCenter();

    public static void main(String[] args) {

        Test test = new Test();
        float count = 0;
        float c = 5/9f;

        if (test.getMostPowerfulShipTest_bestFirePower()) {
            count += c;
            System.out.println("1.1 Test passed");
        }
        else System.out.println("1.1 Test failed");

        if (test.getMostPowerfulShipTest_firstListBestFirePower()) {
            count += c;
            System.out.println("1.2 Test passed");
        }
        else System.out.println("1.2 Test failed");

        if (test.getMostPowerfulShipTest_bestFirePowerNull()) {
            count += c;
            System.out.println("1.3 Test passed");
        }
        else System.out.println("1.3 Test failed");

        System.out.println();

        if (test.getShipByName_uniqueName()) {
            count += c;
            System.out.println("2.1 Test passed");
        }
        else System.out.println("2.1 Test failed");

        if (test.getShipByName_uniqueName_null()) {
            count += c;
            System.out.println("2.2 Test passed");
        }
        else System.out.println("2.2 Test failed");

        System.out.println();

        if (test.getAllShipsWithEnoughCargoSpace_listOfMaximum()) {
            count += c;
            System.out.println("3.1 Test passed");
        }
        else System.out.println("3.1 Test failed");

        if (test.getAllShipsWithEnoughCargoSpace_listOfMaximum_nullList()) {
            count += c;
            System.out.println("3.2 Test passed");
        }
        else System.out.println("3.2 Test failed");

        System.out.println();

        if (test.getAllCivilianShips_unarmed()) {
            count += c;
            System.out.println("4.1 Test passed");
        }
        else System.out.println("4.1 Test failed");

        if (test.getAllCivilianShips_unarmed_nullList()) {
            count += c;
            System.out.println("4.2 Test passed");
        }
        else System.out.println("4.2 Test failed");

        System.out.println("Points: " + count);
    }

    private boolean getMostPowerfulShipTest_bestFirePower () {
        testList.add(new Spaceship("First", 100, 0,0));
        testList.add(new Spaceship("Second", 0, 0,0));

        Spaceship result = commandCenter.getMostPowerfulShip(testList);

        testList.clear();
        return result.getName().equals("First");
    }

    private boolean getMostPowerfulShipTest_firstListBestFirePower () {
        testList.add(new Spaceship("First", 100, 0, 0));
        testList.add(new Spaceship("Second", 100, 0, 0));
        testList.add(new Spaceship("Third", 0, 0, 0));

        Spaceship result = commandCenter.getMostPowerfulShip(testList);

        testList.clear();
        return result.getName().equals("First");
    }

    private boolean getMostPowerfulShipTest_bestFirePowerNull () {
        testList.add(new Spaceship("First", 0, 0, 0));
        testList.add(new Spaceship("Second", 0, 0, 0));

        Spaceship result = commandCenter.getMostPowerfulShip(testList);

        testList.clear();
        return result == null;
    }

    private boolean getShipByName_uniqueName() {
        testList.add(new Spaceship("First", 0, 0, 0));
        testList.add(new Spaceship("Second", 0, 0, 0));

        Spaceship result = commandCenter.getShipByName(testList, "First");

        if (result.getName().equals("First")) {
            testList.clear();
            return true;
        } else {
            testList.clear();
            return false;
        }
    }

    private boolean getShipByName_uniqueName_null() {
        testList.add(new Spaceship("First", 100, 0, 0));

        Spaceship result = commandCenter.getShipByName(testList, "Second");

        testList.clear();
        return result == null;
    }

    private boolean getAllShipsWithEnoughCargoSpace_listOfMaximum() {
        testList.add(new Spaceship("First", 100, 30, 0));
        testList.add(new Spaceship("Second", 100, 0, 0));
        testList.add(new Spaceship("Third", 100, 40, 0));

        ArrayList<Spaceship> result = commandCenter.getAllShipsWithEnoughCargoSpace(testList, 20);

        if (result.get(0).getName().equals("First") & result.get(1).getName().equals("Third")) {
            testList.clear();
            return true;
        } else {
            testList.clear();
            return false;
        }
    }

    private boolean getAllShipsWithEnoughCargoSpace_listOfMaximum_nullList() {
        testList.add(new Spaceship("First", 100, 30, 0));
        testList.add(new Spaceship("Second", 100, 0, 0));
        testList.add(new Spaceship("Third", 100, 40, 0));

        ArrayList<Spaceship> result = commandCenter.getAllShipsWithEnoughCargoSpace(testList, 50);

        if (result.isEmpty()) {
            testList.clear();
            return true;
        } else {
            testList.clear();
            return false;
        }
    }

    private boolean getAllCivilianShips_unarmed() {
        testList.add(new Spaceship("First", 0, 30, 0));
        testList.add(new Spaceship("Second", 100, 0, 0));
        testList.add(new Spaceship("Third", 0, 40, 0));

        ArrayList <Spaceship> result = commandCenter.getAllCivilianShips(testList);

        if (result.get(0).getName().equals("First") & result.get(1).getName().equals("Third")) {
            testList.clear();
            return true;
        } else {
            testList.clear();
            return false;
        }
    }

    private boolean getAllCivilianShips_unarmed_nullList() {
        testList.add(new Spaceship("First", 200, 30, 0));
        testList.add(new Spaceship("Second", 100, 0, 0));
        testList.add(new Spaceship("Third", 100, 40, 0));

        ArrayList <Spaceship> result = commandCenter.getAllCivilianShips(testList);

        return result.isEmpty();
    }
}

